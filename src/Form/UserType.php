<?php

namespace App\Form;

use App\Entity\User;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'lastname',
                TextType::class,
                [
                    'label' => 'name'
                ]
            )
            ->add(
                'firstname',
                TextType::class,
                    [
                        'label' => 'Prénom'
                    ]
            )
            ->add(
                'email',
                TextType::class,
                [
                    'label' => 'Email'
                ]
            )
            ->add(
                'plainPassword',
                // 2 champs qui doivent avoir la même valeur
                RepeatedType::class,
                [
                    //.. de type password
                    'type' => PasswordType::class,
                    'first_options' => [
                        'label'=>'Mot de passe'
                    ],
                    'second_options'=> [
                        'label'=>'Confirmation du mot de passe'
                    ],
                    'invalid_message' =>'Les mot de passe ne correspondent pas '
                ]

            )
            ->add('role')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
