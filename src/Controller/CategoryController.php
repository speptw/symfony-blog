<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryController
 * @package App\Controller
 * @Route("/categorie")
 */
class CategoryController extends AbstractController
{

    /**
     * @Route("/{id}")
     */
    public function index(Category $category)
    {
        return $this->render('category/index.html.twig',
            [
                'category' => $category
            ]
        );
    }
    public function menu(CategoryRepository $categoryRepository)
    {
        $categories = $categoryRepository->findBy(
            [],
            ['name' => 'ASC']
        );

        return $this->render(
            'category/menu.html.twig',
            [
                'categories' => $categories
            ]
        );
    }
}
