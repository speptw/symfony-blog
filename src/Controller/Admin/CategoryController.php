<?php


namespace App\Controller\Admin;

Use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryController
 * @package App\Controller\Admin
 *
 * prefixe de route pour toutes les routes définies dans cette classe
 * @route("/categorie")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(CategoryRepository $repo)
    {
        //lister toutes les categories dans un tableau HTML
        //$repo = $this->getDoctrine()->getRepository(CategoryController::class)
        //ou
        //$em = $this->getDoctrine()->getManager();
        //$repo = $em->getRepository(CategoryRepository::class)
        //$categories = $repo->findAll(); renvoie toutes les infos
        //finAll() avec un tri sur le nom:

        $categories = $repo->findBy([], ['name' => 'ASC']);
        return $this->render(
            'admin/category/index.html.twig',
            [
            'categories' => $categories
            ]
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * La partie variable ID dans la route est optionnelle est vaut
     *
     * @Route("/edition/{id}",
     *     defaults={"id": null},
     *      requirements={"id": "\d+"})
     */
    public function edit(
        Request $request,
        EntityManagerInterface $em,
        $id
    ){
        if (is_null($id)) { //création
            $category = new Category();
        }else { //modification
            $em->find(Categorie::class, $id);
        }
        //404 si l'id n'est pas en bdd
        if (is_null($category)) {
            throw new NotAcceptableHttpException();
        }


        $category = new category();
        // creation de formulaire relié à la catégorie
        $form = $this->createForm(CategoryType::class, $category);

        //le formulaire analyse la requete et fait le mapping
        // avec l'entité s'il a été soumis

        $form -> handleRequest($request);
        //si le formulaire a été soumis
        if ($form->isSubmitted()){
            if ($form->isValid()) {
                //enregistrement en bdd
                $em->persist($category);
                $em->flush();
                $this->addFlash('success', 'La catégorie a bien été enregistrée');

                // redirection vers la liste
            return $this->redirectToRoute('app_admin_category_index');
            }
        }
        return $this->render(
            'admin/category/edit.html.twig',
            [
                //passage du formulaire au template
            'form' => $form->createView()
        ])

        ;
    }

    /**
     * ParamConverter :$category est un objet Category
     * correspondant à la categorie dont on a reçu l'id dans l'url
     * @Route("/supression/{id}")
     */
    public function delete(
        Category $category,
        EntityManagerInterface $em
    ){
        $name = $category->getName();
        $em->remove($category);
        $em->flush();

        $this->addFlash('success', 'La catégorie' . $name . ' a bien été supprimée');
        return $this->redirectToRoute('app_admin_category_index');
    }
}